//
//  DrawingView.m
//  PapersReviewDemo
//
//  Created by jackstevenson on 2018/5/27.
//  Copyright © 2018年 jackstevenson. All rights reserved.
//

#import "DrawingView.h"

#define MAX_RESOLUTION (1920 * 1080)

@interface DrawingView()
@property (nonatomic) CGPoint curPos;
//@property (nonatomic, assign) CGMutablePathRef curPath;
@property (nonatomic,strong) NSMutableArray *pathArray;
@end

@implementation DrawingView
{
    CGMutablePathRef _curPath;
}

-(void)setImage:(UIImage *)image
{
    _image = image;
    self.pathArray = nil;
#warning --性能待优化处
    //真机测试中，过大的图片会导致不明原因的view bug(例如，非常卡顿)，这里不得不压缩图片。
    CGFloat imageSize = image.size.height * image.size.width;
    CGFloat scaleFactor = 1;
    if(imageSize > MAX_RESOLUTION)
    {
        scaleFactor *= sqrt(MAX_RESOLUTION / imageSize);
    }
    if(scaleFactor != 1) _image = [self scaleImage:_image toScale:scaleFactor];
    //self.frame = CGRectMake(self.superview.frame.origin.x, self.superview.frame.origin.y, _image.size.width, _image.size.height);
    self.frame = CGRectMake(0, 0, _image.size.width, _image.size.height);
    self.bounds = CGRectMake(0, 0, _image.size.width, _image.size.height);
    [self setBackgroundColor:[UIColor colorWithPatternImage:_image]];
}

-(void)setViewWithLineWidth:(CGFloat)width lineCap:(CGLineCap)cap joinType:(CGLineJoin)join andColor:(UIColor *)color
{
    self.lineWidth = width;
    self.capType = cap;
    self.joinType = join;
    self.color = color;
}

- (UIColor *)color
{
    if(!_color) _color = [UIColor redColor];
    return _color;
}

- (NSArray *)pathArray
{
    if(!_pathArray) _pathArray = [[NSMutableArray alloc]init];
    return _pathArray;
}

-(void)drawingAt:(CGPoint)pos
{
    self.curPos = pos;
    CGPathAddLineToPoint(_curPath, NULL, self.curPos.x, self.curPos.y);
    [self setNeedsDisplay];
}

- (void)endDrawing
{
    if(_curPath != NULL)
    {
        UIBezierPath *path = [UIBezierPath bezierPathWithCGPath:_curPath];
        [self.pathArray addObject:path];
        CGPathRelease(_curPath);
        _curPath = NULL;
        _curPos = CGPointZero;
        [self setNeedsDisplay];
    }
}

- (void)cancelCurrentDrawing
{
    if(_curPath != NULL)
    {
        CGPathRelease(_curPath);
        _curPath = NULL;
        self.curPos = CGPointZero;
        [self setNeedsDisplay];
    }
}


- (void)drawRect:(CGRect)rect {
    NSLog(@"draw called");
    /*
    if(self.image != nil)
    {
        [self.image drawInRect:rect];//相当消耗资源，造成view刷新率低，同时增加了触控事件捕获间隔，最终导致绘图曲线有太多折线段
        //改设置image为background color
    }*/
    if([self.pathArray count] > 0)
    {
        for(UIBezierPath *path in self.pathArray)
        {
            CGContextRef context = UIGraphicsGetCurrentContext();
            CGContextAddPath(context, path.CGPath);
            [self.color set];
            CGContextSetLineWidth(context, self.lineWidth);
            CGContextSetLineCap(context, kCGLineCapRound);
            CGContextSetLineJoin(context, kCGLineJoinRound);
            CGContextDrawPath(context, kCGPathStroke);
        }
    }
    if(_curPath == NULL && !CGPointEqualToPoint(self.curPos, CGPointZero))
    {
        NSLog(@"create Path");
        _curPath = CGPathCreateMutable();
        CGPathMoveToPoint(_curPath, NULL, self.curPos.x, self.curPos.y);
    }
    else
    {
        CGContextRef context = UIGraphicsGetCurrentContext();
        CGContextAddPath(context, _curPath);
        [self.color set];
        CGContextSetLineWidth(context, self.lineWidth);
        CGContextSetLineCap(context, self.capType);
        CGContextSetLineJoin(context, self.joinType);
        CGContextDrawPath(context, kCGPathStroke);
    }
}

- (UIImage *)scaleImage:(UIImage *)image toScale:(CGFloat)scaleSize
{
    UIGraphicsBeginImageContext(CGSizeMake(image.size.width * scaleSize, image.size.height * scaleSize));
    [image drawInRect:CGRectMake(0, 0, image.size.width * scaleSize, image.size.height * scaleSize)];
    UIImage *scaledImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return scaledImage;
}


@end
