//
//  main.m
//  PapersReviewDemo
//
//  Created by jackstevenson on 2018/5/26.
//  Copyright © 2018年 jackstevenson. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
