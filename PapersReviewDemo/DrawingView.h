//
//  DrawingView.h
//  PapersReviewDemo
//
//  Created by jackstevenson on 2018/5/27.
//  Copyright © 2018年 jackstevenson. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DrawingView : UIView
@property (nonatomic, strong) UIImage *image;
@property (nonatomic) CGFloat lineWidth;
@property (nonatomic) CGLineCap capType;
@property (nonatomic) CGLineJoin joinType;
@property (nonatomic) UIColor *color;
-(void)setViewWithLineWidth:(CGFloat)width lineCap:(CGLineCap)cap joinType:(CGLineJoin)join andColor:(UIColor *)color;
-(void)drawingAt:(CGPoint)pos;
-(void)endDrawing;
-(void)cancelCurrentDrawing;
@end

