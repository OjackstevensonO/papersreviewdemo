//
//  ViewController.m
//  PapersReviewDemo
//
//  Created by jackstevenson on 2018/5/26.
//  Copyright © 2018年 jackstevenson. All rights reserved.
//

#import "ViewController.h"


//定义了两种画线方法
#define USING_TOUCH_MOVING_METHOD
//#define USING_PAN_GESTURE

@interface ViewController () <UIImagePickerControllerDelegate, UINavigationControllerDelegate>
@property (nonatomic, strong) UIImage *image;
@property (nonatomic) BOOL isDrawing;
@property (nonatomic) DrawingView *displayView;
- (IBAction)addImage:(UIBarButtonItem *)sender;
@end

@implementation ViewController



- (UIView *)displayView
{
    if(!_displayView)
    {
        _displayView = [[DrawingView alloc]initWithFrame:self.view.frame];
        [_displayView setBackgroundColor:[UIColor whiteColor]];
        [_displayView setUserInteractionEnabled:YES];
        [_displayView setMultipleTouchEnabled:YES];
        [_displayView setViewWithLineWidth:1.0f lineCap:kCGLineCapRound joinType:kCGLineJoinRound andColor:[UIColor redColor]];
    }
    return _displayView;
}

- (void)setImage:(UIImage *)image
{
    _image = image;
    [self.displayView setImage:image];
    //[self.displayView setNeedsDisplay];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.view addSubview:self.displayView];
    //1.缩放手势
    UIPinchGestureRecognizer *pinchGestureRecognizer = [[UIPinchGestureRecognizer alloc] initWithTarget:self action:@selector(pinchView:)];
    [self.view addGestureRecognizer:pinchGestureRecognizer];
    //2.拖动手势
    UIPanGestureRecognizer *panGestureRecognizer = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(panView:)];
    panGestureRecognizer.minimumNumberOfTouches = 2;
    panGestureRecognizer.maximumNumberOfTouches = 2;
    [self.view addGestureRecognizer:panGestureRecognizer];
#ifdef USING_PAN_GESTURE
    //3.绘图手势
    UIPanGestureRecognizer *drawPanGestureRecognizer = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(drawPanView:)];
    drawPanGestureRecognizer.minimumNumberOfTouches = 1;
    drawPanGestureRecognizer.maximumNumberOfTouches = 1;
    [self.view addGestureRecognizer:drawPanGestureRecognizer];
#endif
    self.isDrawing = NO;
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (IBAction)addImage:(UIBarButtonItem *)sender {
    UIImagePickerController *ipc = [[UIImagePickerController alloc]init];
    ipc.delegate = self;
    ipc.allowsEditing = YES;
    ipc.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    [self presentViewController:ipc animated:YES completion:^{
        
    }];
}

-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<NSString *,id> *)info
{
    self.image = info[UIImagePickerControllerOriginalImage];
    [self dismissViewControllerAnimated:YES completion:^{
        
    }];
}


//缩放手势实现
- (void) pinchView:(UIPinchGestureRecognizer *)pinchGestureRecognizer
{
    //对于本想缩放但误触为drawing的情况进行清理
    if(self.isDrawing == YES)
    {
        self.isDrawing = NO;
        [self.displayView cancelCurrentDrawing];
    }
    NSLog(@"pinch");
    //UIView *view = pinchGestureRecognizer.view;
    if (pinchGestureRecognizer.state == UIGestureRecognizerStateBegan || pinchGestureRecognizer.state == UIGestureRecognizerStateChanged) {
        self.displayView.transform = CGAffineTransformScale(self.displayView.transform, pinchGestureRecognizer.scale, pinchGestureRecognizer.scale);
        pinchGestureRecognizer.scale = 1;
    }
}

//拖动手势实现
- (void) panView:(UIPanGestureRecognizer *)panGestureRecognizer
{
    //对于本想拖动但误触为drawing的情况进行清理
    if(self.isDrawing == YES)
    {
        self.isDrawing = NO;
        [self.displayView cancelCurrentDrawing];
    }
    NSLog(@"pan");
    UIView *view = panGestureRecognizer.view;
    if (panGestureRecognizer.state == UIGestureRecognizerStateBegan || panGestureRecognizer.state == UIGestureRecognizerStateChanged) {
        CGPoint translation = [panGestureRecognizer translationInView:view.superview];
        [self.displayView setCenter:(CGPoint){self.displayView.center.x + translation.x, self.displayView.center.y + translation.y}];
        [panGestureRecognizer setTranslation:CGPointZero inView:self.displayView.superview];
    }
}

//单指拖动-->划线 实现
#ifdef USING_PAN_GESTURE
- (void) drawPanView:(UIPanGestureRecognizer *)panGestureRecognizer
{
    if (panGestureRecognizer.state == UIGestureRecognizerStateBegan || panGestureRecognizer.state == UIGestureRecognizerStateChanged) {
        NSLog(@"moving");
        self.isDrawing = YES;
        [self.displayView drawingAt:[panGestureRecognizer locationInView:self.displayView]];
    }
    else if(panGestureRecognizer.state == UIGestureRecognizerStateEnded)
    {
        NSLog(@"end");
        self.isDrawing = NO;
        [self.displayView endDrawing];
    }
    else if(panGestureRecognizer.state == UIGestureRecognizerStateCancelled)
    {
        self.isDrawing = NO;
        [self.displayView cancelCurrentDrawing];
    }
}
#endif


#ifdef USING_TOUCH_MOVING_METHOD
- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    NSLog(@"touch began");
    if([touches count] == 1 && self.isDrawing == NO)
    {
        self.isDrawing = YES;
        UITouch *touch = [touches anyObject];
        [self.displayView drawingAt:[touch locationInView:self.displayView]];
    }
}

- (void)touchesMoved:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    NSLog(@"touch moving");
    if([touches count] == 1 && self.isDrawing == YES)
    {   //从moved后开始画线有利于减少误画线，例如避免对tap进行误drawing
        UITouch *touch = [touches anyObject];
        [self.displayView drawingAt:[touch locationInView:self.displayView]];
    }
    else if([touches count] > 1)
    {   //错误处理(划线过程中出现多个手指，说明可能是其他手势)
        [self.displayView cancelCurrentDrawing];
        self.isDrawing = NO;
    }
}

- (void)touchesEnded:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    if([touches count] == 1) [self.displayView endDrawing];
    self.isDrawing = NO;
}

- (void)touchesCancelled:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    if([touches count] == 1) [self.displayView cancelCurrentDrawing];
    self.isDrawing = NO;
}
#endif

@end
